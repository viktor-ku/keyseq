extern crate linux_raw_input_rs;

use linux_raw_input_rs::{
  get_input_devices,
  InputReader,
  keys::Keys,
  input,
};

use std::time::SystemTime;
use std::fmt;

fn now() -> u128 {
  SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis()
}

#[derive(Debug)]
enum EventType {
  Release,
  Push,
  Hold,
}

struct PressEvent {
  date: u128,
  key: Keys,
  event: EventType,
}

impl PressEvent {
  pub fn new(key: Keys, event: input::EventType) -> Self {
    Self {
      date: now(),
      key,
      event: match event {
        input::EventType::Push => EventType::Push,
        input::EventType::Release => EventType::Release,
        input::EventType::Other(_) => EventType::Hold,
      },
    }
  }
}

impl fmt::Display for PressEvent {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}, {:#?}, {:#?}", self.date, self.key, self.event)
  }
}

fn main() {
  let device_path = get_input_devices().first().unwrap().clone();
  let mut reader = InputReader::new(device_path);

  loop {
    let input = reader.current_state();

    if input.is_key_event() {
      let e = PressEvent::new(input.get_key(), input.event_type());
      println!("{}", e);
    }
  }
}
